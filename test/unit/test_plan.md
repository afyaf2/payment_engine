**Test Plan**

1. Client Controller Unit Tests:
   
   A. Valid Cases
   
          - Withdraw
             - Observe state change Total/Available - Amount
          - Deposit
              - Observe state change Total/Available + Amount
          - Dispute
              - Observe state change:
                 1. Add transaction to client's disputed transactions
                 2. Available - amount disputed, Held + amount disputed 
          - Resolve
              - Observe state change - withdrawal:
                - Held - amount resolved, total = held + available
              - Observe state change - Deposit:
                - Held - amount resolved, total = held + available
              - In both cases observe state change - transaction deleted from disputed_transactions
          - Chargeback
              - Observe state change - withdrawal:
                - Total += chargeback amount
              - Observe state change - Deposit:
                - Total -= chargeback_amount
             - Observe state change `Locked = True`
            
          - Boundary Value Analysis
            - Client_id can be:
              0, 32768, 65535 
            - Available/Held/Total:
              N.0000, N.5555, N.9999 
          
   B. Invalid Cases 
   
          - Chargeback
              - Observe no state change if no dispute registered for given transaction_id
              - Observe no state change if chargeback amount exceeds client total or held funds 
          - Resolve
              - Observe no state change is transaction not found in client's disputed transactions
              - Observe no state change - withdrawal:
                1. Held - amount resolved, total = held + available
              - Observe state change - Deposit:
                1. Held - amount resolved, total = held + available
          - Withdraw
              - Observe no state change if available < transaction amount, transaction added to clients failed transactions list

2. Transaction Controller Unit Tests:
   
      A. Valid Cases
   
          - Chargeback
              - Observe MockClient.make_chargeback called once
          - Deposit
              - Observe MockClient.make_deposit called once
          - Dispute
              - Observe MockClient.make_dispute called once
          - Resolve
              - Observe MockClient.make_resolve called once
          - Withdraw
              - Observe MockClient.make_withdrawal called once 
        
          - Boundary Value Analysis
            - Client_id:
              0, 32768, 65535 
            - Transaction ID:
              0, 2147483648, 4294967295 
            - Amount 
              - 1 d.p. N.0, N.5, N.9 
              - 2 d.p. N.00, N.55, N.99 
              - 3 d.p. N.000, N.555, N.999 
              - 4 d.p. N.0000, N.5555, N.9999 
          
  B. Invalid Cases
  
      - Process_Transaction
          - If client account is locked, return client
          - If transaction type is invalid, return client
          
      - For Each Make Chargeback, Dispute, Resolve, Withdrawal
          - If transaction record (row) not found return client
      
      - Boundary Value Analysis (Exceeded Bounds)
        - Client_id:
          -1, 65536 
        - Transaction ID:
          -1, 4294967296 
        - Amount  
          - 5 d.p. N.10000 
   
3. Process Transactions Command Unit Tests:

        Case 1: Client Makes Deposit
        Case 2: Client Makes Withdrawal
        Case 3: Client Makes Dispute
        Case 4: Client Dispute is Resolved
        Case 5: Multiple Clients Happy Path (Deposit & Withdraw)
        Case 6: Multiple Clients Perform All Actions Besides Chargeback
        Case 7: Multiple Clients Perform All Actions, One Client Chargeback & Attempts Further Action but Frozen