import pathlib
from csv import DictReader


class TransactionController(object):
    def __init__(self, args):
        """
        Initializes a new instance of the Transaction Controller class
        Keyword arguments:
        :param args: Array containing the following arguments:
        [0] Transaction_Type: String
        [1] Client: Instance of Client Controller
        [2] Transaction ID: Integer
        [3] Amount: Float
        [4] Transactions List: Filepath for transactions csv to be used in event of dispute
        """

        if len(args) != 5:
            raise ValueError("Arguments provided to TransactionController are invalid.")

        self.transaction_type: str = args[0]
        self.client: object = args[1]
        self.transaction_id: int = args[2]
        self.amount: [float, None] = float(args[3]) if args[3] else None
        self.transactions_list: pathlib.PosixPath = args[4]

    def process_transaction(self) -> object:
        transaction_types = {'chargeback', 'deposit', 'dispute', 'resolve', 'withdrawal'}

        # If preventing malicious queries was a real priority I could raise a ValueError here and log the id
        # for the or condition but to ensure the engine continues to process transactions I will just return client
        if self.client.locked or self.transaction_type not in transaction_types:
            return self.client

        if self.transaction_type == "chargeback":
            client = self.make_chargeback()
            return client
        if self.transaction_type == "deposit":
            client = self.make_deposit()
            return client
        if self.transaction_type == "dispute":
            client = self.make_dispute()
            return client
        if self.transaction_type == "resolve":
            client = self.make_resolve()
            return client
        if self.transaction_type == "withdrawal":
            client = self.make_withdrawal()
            return client

    def make_chargeback(self) -> object:
        row, chargeback_amount = self._find_transaction(self.client, self.transaction_id, self.transactions_list)
        if row:
            self.client.chargeback(row, chargeback_amount, self.transaction_id)
        return self.client

    def make_deposit(self) -> object:
        self.client.deposit_funds(self.amount)
        return self.client

    def make_dispute(self) -> object:
        row, amount_disputed = self._find_transaction(self.client, self.transaction_id, self.transactions_list)
        if row:
            self.client.begin_dispute(row, amount_disputed, self.transaction_id)
        return self.client

    def make_resolve(self) -> object:
        row, amount_resolved = self._find_transaction(self.client, self.transaction_id, self.transactions_list)
        if row:
            self.client.resolve_dispute(row, amount_resolved, self.transaction_id)
        return self.client

    def make_withdrawal(self) -> object:
        row = self._find_transaction(self.client, self.transaction_id, self.transactions_list)
        if row:
            self.client.withdraw_funds(row, self.amount, self.transaction_id)
        return self.client

    @staticmethod
    def _find_transaction(client, transaction_id, transactions_list) -> [object, float]:
        with open(transactions_list) as csv:
            csv = DictReader(csv, skipinitialspace=True)
            for row in csv:
                if client.client_id == row['client'] and transaction_id == row['tx'] \
                        and row['type'] in ['deposit', 'withdrawal']:
                    return row, float(row['amount'])
            return None, None
