import argparse
from pathlib import Path

from src.engine_commands.process_transactions_command import ProcessTransactionsCommand


def run(transactions_csv):
    command = ProcessTransactionsCommand(transactions_csv)
    command.execute()


def setup():
    arg_parser = argparse.ArgumentParser(description='CSV Payments Engine')
    arg_parser.add_argument("transactions_list", help="Processes the provided Transactions List at path/to/file.csv")
    args = arg_parser.parse_args()
    return _prepare_csv(args.transactions_list)


def _prepare_csv(file_name):
    if not file_name.endswith(".csv"):
        raise Exception('Provided file must be a CSV')
    csv = Path(Path.cwd(), file_name)
    if not Path.exists(csv):
        raise FileNotFoundError(f"Error - csv file not found at {csv}")
    return csv


if __name__ == "__main__":
    transactions_csv = setup()
    run(transactions_csv)
