import unittest
from ddt import data, ddt, unpack
from mock import patch, mock_open
from src.engine_commands.process_transactions_command import ProcessTransactionsCommand


@ddt
class ProcessTransactionsCommandTest(unittest.TestCase):

    @unpack
    @data(("Case 1: Client Makes Deposit",
           "type,client,tx,amount\n"
           "deposit, 1, 1, 1.0",
           ["1,1.0,0.0,1.0,False"]),
          ("Case 2: Client Makes Withdrawal",
           "type,client,tx,amount\n"
           "deposit, 1, 1, 3.0\n"
           "withdrawal, 1, 2, 1.0",
           ["1,2.0,0.0,2.0,False"]),
          ("Case 3: Client Makes Dispute",
           "type,client,tx,amount\n"
           "deposit, 1, 1, 5.0\n"
           "dispute, 1, 1",
           ["1,0.0,5.0,5.0,False"]),
          ("Case 4: Client Dispute is Resolved",
           "type,client,tx,amount\n"
           "deposit, 1, 1, 5.0\n"
           "dispute, 1, 1\n"
           "resolve, 1, 1",
           ["1,0.0,0.0,0.0,False"]),
          ("Case 5: Multiple Clients Happy Path (Deposit & Withdraw)",
           "type,client,tx,amount\n"
           "deposit, 1, 1, 5.0\n"
           "deposit, 2, 2, 3.0\n"
           "withdrawal, 1, 3, 1.0\n"
           "withdrawal, 2, 4, 2.5",
           ["1,4.0,0.0,4.0,False", "2,0.5,0.0,0.5,False"]),
          ("Case 6: Multiple Clients Perform All Actions Besides Chargeback",
           "type,client,tx,amount\n"
           "deposit, 1, 1, 5.0\n"
           "deposit, 2, 2, 3.0\n"
           "dispute, 1, 1\n"
           "dispute, 2, 2\n"
           "resolve, 1, 1\n"
           "resolve, 2, 2\n",
           ["1,0.0,0.0,0.0,False", "2,0.0,0.0,0.0,False"]),
          ("Case 7: Multiple Clients Perform All Actions, One Client Chargeback & Attempts Further Action but Frozen",
           "type,client,tx,amount\n"
           "deposit, 1, 1, 10.0\n"
           "withdrawal, 1, 2, 4.5\n"
           "deposit, 2, 3, 5.0\n"
           "dispute, 1, 2\n"
           "chargeback, 1, 2\n"
           "deposit, 1, 4, 5.0\n",
           ["1,5.5,0.0,10.0,True", "2,5.0,0.0,5.0,False"]))
    def test_process_transactions_client_accounts_outputted(self, case, csv_data, expected_values):
        # Arrange
        expected_header = "client,available,held,total,locked"

        if type(expected_values) == list:
            formatted_values = []
            for expected in expected_values:
                formatted_values.append("".join([str(x) for x in expected]))
                expected_values = formatted_values

        csv_mock = mock_open(read_data=csv_data)
        command = ProcessTransactionsCommand(csv_data)

        # Act
        with patch("builtins.open", csv_mock), \
             patch("sys.stdout") as client_accounts_csv:
            command.execute()

        # Assert
        for index, row in enumerate(client_accounts_csv.method_calls):
            actual = row[1][0].replace("\r\n", "")
            if index is 0:
                self.assertEqual(expected_header, actual)
            else:
                self.assertEqual(expected_values[index-1], actual, f"{case} failed - {expected_values} does not match {actual}")