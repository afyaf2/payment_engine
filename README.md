# README #

Payment Engine 

### What is this repository for? ###

This is payment engine that takes an input of path/to/file.csv of valid transactions, and outputs a CSV to stdout of client account states.

I chose to implement (not entirely canonically) the command pattern as that is what I am most familiar with when developing CLIs.

Supports all required transaction types.

I had intended to implement Multiprocessing (_not_ Multithreading) in the process_transactions_command but due to using a class it wasn't looking super tidy, and moving the buffer splitting to 
payment_engine.py would have caused me to completely change the implementation of the Process Transactions Command.

I added a "failed transactions" dict to Clients, as without it a malicious actor could have attempted to dispute a transaction on the ledger that didn't actually happen (like a failed withdrawal).

I think for future SDET take-homes, it would be nice if the base features were implemented, maybe with some bugs, so that more time can be spent writing interesting and fun tests,
I'd have loved some time to create a Chaos Engine that generated terrifyingly large and poorly formatted CSVs to really stretch the limits of the Payments Engine. In any case you can view my test 
plan in test/unit/test_plan.md and the data I used to test the engine in test/fixtures.

### How do I get set up? ###

* CLI runs as per requirements `python3 payment_engine.py path/to/transactions.csv` with the option to add `> client_accounts.csv` for stdout.
* No Config required
* Uses only builtin python modules so no venv or requirements.txt - However I wrote this using Python 3.7

### How do I run Tests? ###

* Requires installation of DDT and Mock packages to run the main Process Transactions Command smoke tests

### Who do I talk to? ###

Thanks for taking the time to look at this

Feedback welcomed -> awfrej@gmail.com

       ▄████████    ▄████████ ▄██   ▄      ▄████████    ▄████████       ▄█     █▄     ▄████████  ▄██████▄      ███        ▄████████          ███        ▄█    █▄     ▄█     ▄████████ 
      ███    ███   ███    ███ ███   ██▄   ███    ███   ███    ███      ███     ███   ███    ███ ███    ███ ▀█████████▄   ███    ███      ▀█████████▄   ███    ███   ███    ███    ███ 
      ███    ███   ███    █▀  ███▄▄▄███   ███    ███   ███    █▀       ███     ███   ███    ███ ███    ███    ▀███▀▀██   ███    █▀          ▀███▀▀██   ███    ███   ███▌   ███    █▀  
      ███    ███  ▄███▄▄▄     ▀▀▀▀▀▀███   ███    ███  ▄███▄▄▄          ███     ███  ▄███▄▄▄▄██▀ ███    ███     ███   ▀  ▄███▄▄▄              ███   ▀  ▄███▄▄▄▄███▄▄ ███▌   ███        
    ▀███████████ ▀▀███▀▀▀     ▄██   ███ ▀███████████ ▀▀███▀▀▀          ███     ███ ▀▀███▀▀▀▀▀   ███    ███     ███     ▀▀███▀▀▀              ███     ▀▀███▀▀▀▀███▀  ███▌ ▀███████████ 
      ███    ███   ███        ███   ███   ███    ███   ███             ███     ███ ▀███████████ ███    ███     ███       ███    █▄           ███       ███    ███   ███           ███ 
      ███    ███   ███        ███   ███   ███    ███   ███             ███ ▄█▄ ███   ███    ███ ███    ███     ███       ███    ███          ███       ███    ███   ███     ▄█    ███ 
      ███    █▀    ███         ▀█████▀    ███    █▀    ███              ▀███▀███▀    ███    ███  ▀██████▀     ▄████▀     ██████████         ▄████▀     ███    █▀    █▀    ▄████████▀  
                                                                                     ███    ███                                                                                       