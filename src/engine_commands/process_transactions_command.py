import pathlib
import sys
from csv import DictWriter, DictReader

from src.controllers.transaction_controller import TransactionController
from src.controllers.client_controller import Client


class ProcessTransactionsCommand(object):

    def __init__(self, transactions_list):
        self.transactions_csv: pathlib.PosixPath = transactions_list

    def execute(self) -> DictWriter:
        clients = dict()

        with open(self.transactions_csv) as csv:
            data = DictReader(csv, skipinitialspace=True)
            for row in data:
                client_id = row['client']

                if len(clients) == 0:
                    clients[client_id] = (Client(client_id))

                lookup_client = clients.get(client_id)
                if lookup_client:
                    transaction = TransactionController([row['type'], lookup_client, row['tx'],
                                                         row['amount'], self.transactions_csv])
                elif not lookup_client:
                    new_client = Client(client_id)
                    transaction = TransactionController([row['type'], new_client, row['tx'],
                                                         row['amount'], self.transactions_csv])

                updated_client = transaction.process_transaction()
                clients[client_id] = updated_client

        try:
            writer = DictWriter(sys.stdout, fieldnames=[
                "client", "available", "held", "total", "locked"
            ])
            writer.writeheader()
            for client in clients.items():
                writer.writerow({"client": client[1].client_id,
                                 "available": float(format(client[1].available, '.4f')),
                                 "held":  float(format(client[1].held, '.4f')),
                                 "total":  float(format(client[1].total, '.4f')),
                                 "locked": client[1].locked})
            return writer
        except Exception as e:
            print(f"ERROR: {e} while writing CSV")
