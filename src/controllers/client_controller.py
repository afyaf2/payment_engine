class Client:
    def __init__(self, args):
        """
        Initializes a new instance of the Client Controller class
        Keyword arguments:
        :param args: Array containing the following arguments:
        [0] Client_ID: String
        [1] Available: Available Funds
        [2] Held: Funds under hold
        [3] Total: Total funds (available + held)
        [4] Locked: Boolean - client has completed a chargeback and further action is restricted
        [5] Disputed Transactions: Transactions under dispute associated with client
        [6] Failed transactions: Transactions failed by client, not eligible for chargeback
        """
        if len(args) != 1:
            raise ValueError("Arguments provided to ClientController are invalid. Transaction Records must have a valid"
                             "Client_ID")

        self.client_id: int = args[0]
        self.available: float = float(0)
        self.held: float = float(0)
        self.total: float = float(0)
        self.locked: bool = False
        self.disputed_transactions: dict = {}
        self.failed_transactions: dict = {}

    def begin_dispute(self, transaction, amount_disputed, transaction_id):
        if self.failed_transactions.get(transaction_id) is None and transaction:
            self.disputed_transactions[transaction_id] = transaction
            self.held += amount_disputed

            # If the client is disputing a withdrawal, we don't want to reduce their available funds.
            if transaction['type'] == "withdrawal":
                return self

            self.available -= amount_disputed

        return self

    def chargeback(self, transaction, chargeback_amount, transaction_id):
        # There should be a hold on the client's account and a dispute registered, if not - don't proceed
        if self.disputed_transactions.get(transaction_id) is not None and \
                transaction == self.disputed_transactions.get(transaction_id):

            if self.total < chargeback_amount or self.held < chargeback_amount:
                return self

            # If a chargeback is a withdrawal - surely there should be an alternative rule - we should credit the
            # account or do we assume they pull their money from the bank thus reducing their Total?
            if transaction['type'] == "withdrawal":
                self.total += chargeback_amount
            elif transaction['type'] == "deposit":
                self.total -= chargeback_amount

            self.held -= chargeback_amount
            self.locked = True
        return self

    def deposit_funds(self, amount):
        formatted_amount = float(format(amount, '.4f'))
        self.available += formatted_amount
        self.total += formatted_amount
        return self

    def resolve_dispute(self, transaction, amount_resolved, transaction_id):
        if self.disputed_transactions.get(transaction_id) is not None and \
                transaction == self.disputed_transactions[transaction_id]:
            if self.disputed_transactions[transaction_id]['type'] == 'withdrawal':
                self.available += amount_resolved
            self.held -= amount_resolved
            self.total = self.held + self.available
            del self.disputed_transactions[transaction_id]
        return self

    def withdraw_funds(self, transaction, amount, transaction_id):
        if self.available < amount or self.total < amount:
            self.failed_transactions[transaction_id] = transaction
            return self
        formatted_amount = float(format(amount, '.4f'))
        self.available -= formatted_amount
        self.total -= formatted_amount
        return self
